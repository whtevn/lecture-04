<?php
  session_start();
  if(IsSet($_POST['user_input'])){
    $_SESSION['input_record'][] = $_POST['user_input'];
  }
?><!DOCTYPE html>
<html>
  <body>
    <a href="..">example home</a> | <a href="../reset.php">reset</a>
    <form method="POST">
      <input type="text" name="user_input" />
      <button type="submit">
        submit
      </button>
    </form>
    <ul>
      <?php foreach($_SESSION['input_record'] as $user_input){ ?>
        <li><?=$user_input?></li>
      <?php } ?>
    </ul>
  </body>
</html>

