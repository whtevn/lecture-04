<?php
  $users = json_decode(file_get_contents("./users.json"), true);
?><!DOCTYPE html>
<html>
  <body>
    <ul>
      <?php foreach($users as $index=>$user){ ?>
        <li>user <?=$index?>: <?= $user["name"] ?> - <?= $user["email"] ?> </li>
      <?php } ?>
    </ul>
  </body>
</html>
